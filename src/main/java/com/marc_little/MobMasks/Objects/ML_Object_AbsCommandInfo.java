package com.marc_little.MobMasks.Objects;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public abstract class ML_Object_AbsCommandInfo {

    private String command;
    private List<String> alias;
    private ML_Object_Permission permission;
    private String description;

    public ML_Object_AbsCommandInfo(String command, String description, List<String> alias, ML_Object_Permission permission) {
        this.command = command;
        this.description = description;
        this.alias = alias;
        this.permission = permission;
    }

    public String getCommand() {
        return this.command;
    }

    public String getDescription() {
        return this.description;
    }

    public List<String> getAlias() {
        return this.alias;
    }

    public String getPermission() {
        return this.permission.getPermission();
    }

    public boolean isDefaultPermission() {
        return this.permission.isDefaultPerm();
    }

    public boolean hasPermission(CommandSender sender) {
        return this.permission.hasPermission(sender);
    }

    public abstract ML_Object_CommandReturn executePlayer(Player player, String[] args);

    public abstract ML_Object_CommandReturn executeOther(CommandSender commandSender, String[] args);

    public ML_Object_Permission getMLPermission() {
        return this.permission;
    }
}

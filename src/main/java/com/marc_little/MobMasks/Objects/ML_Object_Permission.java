package com.marc_little.MobMasks.Objects;

import org.bukkit.command.CommandSender;

public class ML_Object_Permission {

    private final String permission;
    private final boolean defaultPerm;

    public ML_Object_Permission(String permission, boolean defaultPerm) {
        this.permission = permission;
        this.defaultPerm = defaultPerm;
    }

    public String getPermission() {
        return this.permission;
    }

    public boolean isDefaultPerm() {
        return this.defaultPerm;
    }

    public boolean hasPermission(CommandSender sender) {
        if (!this.defaultPerm && !sender.hasPermission(this.permission)) {
            return false;
        }
        return true;
    }
}

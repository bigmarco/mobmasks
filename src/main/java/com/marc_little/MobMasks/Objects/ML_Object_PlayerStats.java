package com.marc_little.MobMasks.Objects;

import org.bukkit.SkullType;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public class ML_Object_PlayerStats {

    Player player;
    int blaze_heads, spider_heads, zombie_heads, pig_heads, sheep_heads, cow_heads, chicken_heads, squid_heads, ocelot_heads;

    public ML_Object_PlayerStats(int blaze_heads, int spider_heads, int zombie_heads, int pig_heads, int sheep_heads, int cow_heads, int chicken_heads, int squid_heads, int ocelot_heads) {
        this.blaze_heads = blaze_heads;
        this.spider_heads = spider_heads;
        this.zombie_heads = zombie_heads;
        this.pig_heads = pig_heads;
        this.sheep_heads = sheep_heads;
        this.cow_heads = cow_heads;
        this.chicken_heads = chicken_heads;
        this.squid_heads = squid_heads;
        this.ocelot_heads = ocelot_heads;
    }

    public int getBlaze_heads() {
        return blaze_heads;
    }

    public int getChicken_heads() {
        return chicken_heads;
    }

    public int getCow_heads() {
        return cow_heads;
    }

    public int getOcelot_heads() {
        return ocelot_heads;
    }

    public int getPig_heads() {
        return pig_heads;
    }

    public int getSheep_heads() {
        return sheep_heads;
    }

    public int getSpider_heads() {
        return spider_heads;
    }

    public int getSquid_heads() {
        return squid_heads;
    }

    public int getZombie_heads() {
        return zombie_heads;
    }

    public Player getPlayer() {
        return player;
    }
}

package com.marc_little.MobMasks.Objects;

import com.marc_little.MobMasks.Enumerators.ML_Enum_CommandResponse;

public class ML_Object_CommandReturn {

    private final ML_Enum_CommandResponse response;
    private final String message;

    public ML_Object_CommandReturn(ML_Enum_CommandResponse response, String message) {
        this.response = response;
        this.message = message;
    }

    public ML_Enum_CommandResponse getResponse() {
        return this.response;
    }

    public String getMessage() {
        return this.message;
    }
}

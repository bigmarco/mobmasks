package com.marc_little.MobMasks.Database;

import com.marc_little.MobMasks.Objects.ML_Object_PlayerStats;
import com.marc_little.MobMasks.Utils.ML_Config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

public class ML_Database {

    private Connection connection;

    private String host, database, username, password;
    private int port;

    private String initString = "CREATE TABLE IF NOT EXISTS `data` (" +
            "  `uuid` varchar(255) NOT NULL DEFAULT ''," +
            "  `blaze` int(11) DEFAULT '0'," +
            "  `spider` int(11) DEFAULT '0'," +
            "  `zombie` int(11) DEFAULT '0'," +
            "  `pig` int(11) DEFAULT '0'," +
            "  `sheep` int(11) DEFAULT '0'," +
            "  `cow` int(11) DEFAULT '0'," +
            "  `chicken` int(11) DEFAULT '0'," +
            "  `squid` int(11) DEFAULT '0'," +
            "  `ocelot` int(11) DEFAULT '0'," +
            "  PRIMARY KEY (`uuid`)" +
            ") ENGINE=InnoDB DEFAULT CHARSET=latin1;";

    public ML_Database() {
        this.host = ML_Config.DB_HOST;
        this.database = ML_Config.DB_DATABASE;
        this.username = ML_Config.DB_USERNAME;
        this.password = ML_Config.DB_PASSWORD;
        this.port = ML_Config.DB_PORT;

        connection = getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(initString);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void openConnection() throws SQLException, ClassNotFoundException{
        if (connection != null && !connection.isClosed()) {
            return;
        }

        synchronized (this) {
            if (connection != null && !connection.isClosed()) {
                return;
            }
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + this.host+ ":" + this.port + "/" + this.database, this.username, this.password);
        }
    }

    public Connection getConnection() {
        try {
            if (connection == null || connection.isClosed()) {
                try {
                    openConnection();
                    return connection;
                }catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
            return connection;
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void savePlayer(UUID player, ML_Object_PlayerStats stats) {
        //do save here?
    }

    public ML_Object_PlayerStats getPlayer(UUID player) {
        //do load here?
        return null;
    }
}

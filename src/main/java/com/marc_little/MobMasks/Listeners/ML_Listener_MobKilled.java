package com.marc_little.MobMasks.Listeners;

import com.marc_little.MobMasks.Enumerators.ML_Enum_MobHeads;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class ML_Listener_MobKilled implements Listener {

    //drop head on entity kill... maybe add drop chance in future
    @EventHandler
    public void onPlayerKillMobEvent(EntityDeathEvent e) {
        if (e.getEntityType() == EntityType.PLAYER) {
            return;
        }
        if (e.getEntity().getKiller() == null) {
            return;
        }
        if (!ML_Enum_MobHeads.hasMobHead(e.getEntityType())) {
            return;
        }
        ML_Enum_MobHeads head = ML_Enum_MobHeads.getMobHead(e.getEntityType());
        e.getDrops().add(head.getHead());
    }

}

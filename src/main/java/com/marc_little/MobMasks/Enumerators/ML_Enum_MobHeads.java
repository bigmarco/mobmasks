package com.marc_little.MobMasks.Enumerators;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.HashMap;

public enum  ML_Enum_MobHeads {
    BLAZE("MHF_Blaze", "Blaze Head", EntityType.BLAZE),
    SPIDER("MHF_Spider", "Spider Head", EntityType.SPIDER),
    ZOMBIE(SkullType.ZOMBIE, EntityType.ZOMBIE),
    PIG("MHF_Pig", "Pig Head", EntityType.PIG),
    SHEEP("MHF_Sheep", "Sheep Head", EntityType.SHEEP),
    COW("MHF_Cow", "Cow Head", EntityType.COW),
    CHICKEN("MHF_Chicken", "Chicken Head", EntityType.CHICKEN),
    SQUID("MHF_Squid", "Squid Head", EntityType.SQUID),
    OCELOT("MHF_Ocelot", "Ocelot Head", EntityType.OCELOT);

    private static HashMap<EntityType, ML_Enum_MobHeads> TYPE_MAP = new HashMap<>();

    ItemStack head;
    EntityType entityType;

    ML_Enum_MobHeads(String headUsername, String itemName, EntityType entityType) {
        head = new ItemStack(Material.SKULL_ITEM, 1, (short)SkullType.PLAYER.ordinal());
        SkullMeta skullMeta = (SkullMeta) head.getItemMeta();
        skullMeta.setOwner(headUsername);
        skullMeta.setDisplayName(ChatColor.RESET + itemName);
        head.setItemMeta(skullMeta);
        this.entityType = entityType;
    }

    ML_Enum_MobHeads(SkullType type, EntityType entityType) {
        head = new ItemStack(Material.SKULL_ITEM, 1, (short)type.ordinal());
        this.entityType = entityType;
    }


    static {
        for (ML_Enum_MobHeads head : values()) {
            TYPE_MAP.put(head.getEntityType(), head);
        }
    }

    public static boolean hasMobHead(EntityType entityType) {
        System.out.println(TYPE_MAP);
        return TYPE_MAP.containsKey(entityType);
    }

    public static ML_Enum_MobHeads getMobHead(EntityType entityType) {
        return TYPE_MAP.get(entityType);
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public ItemStack getHead() {
        return head;
    }


}

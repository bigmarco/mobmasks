package com.marc_little.MobMasks;

import com.marc_little.MobMasks.Database.ML_Database;
import com.marc_little.MobMasks.Managers.ML_Manager_Command;
import com.marc_little.MobMasks.Managers.ML_Manager_Listener;
import com.marc_little.MobMasks.Managers.ML_Manager_MobMasks;
import com.marc_little.MobMasks.Utils.ML_Config;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.jar.JarFile;

public class ML_MobMasks extends JavaPlugin {

    public static String NAME;
    public static String VERSION;
    public static int BUILD_NUMBER;
    public static Date BUILD_DATE;
    public static String AUTHOR;

    private static ML_MobMasks instance;

    private ML_Manager_MobMasks manager_mobMasks;
    private ML_Database database;

    @Override
    public void onEnable() {
        instance = this;

        try {
            NAME = this.getDescription().getName();
            VERSION = this.getDescription().getVersion().split("_")[0];
            BUILD_NUMBER = Integer.parseInt(this.getDescription().getVersion().split("_")[1].substring(1));
            BUILD_DATE = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").parse(this.getDescription().getVersion().split("_")[2].substring(1));
            AUTHOR = this.getDescription().getAuthors().get(0);
        }catch (Exception e) {
            Bukkit.getServer().getConsoleSender().sendMessage("Don't try stealing my plugins :P");
            Bukkit.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        JarFile jar;
        try {
            String path = this.getDataFolder().getAbsolutePath();
            path = path + "-"+ ML_MobMasks.VERSION + ".jar";
            System.out.println(path);
            jar = new JarFile(path);
        }catch (Exception e) {
            e.printStackTrace();
            return;
        }
        ML_Config.reload(false);
        //registerGlow(); later so mobmasks have enchanted glow but might not be needed if their abilities are enchants
        manager_mobMasks = new ML_Manager_MobMasks(this);
        database = new ML_Database();
        this.getCommand("mobmasks").setExecutor(new ML_Manager_Command(this, jar, this.getClassLoader()));
        new ML_Manager_Listener(this, jar, this.getClassLoader());
        try {
            jar.close();
        }catch (Exception e) {
            Bukkit.getServer().getConsoleSender().sendMessage("Don't try stealing my plugins :P");
            Bukkit.getServer().getPluginManager().disablePlugin(this);
        }
    }

    @Override
    public void onDisable() {

    }

    private void registerGlow() {

    }

    public static ML_MobMasks getInstance() {
        return instance;
    }
}

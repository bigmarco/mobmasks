package com.marc_little.MobMasks.Commands;

import com.marc_little.MobMasks.Enumerators.ML_Enum_CommandResponse;
import com.marc_little.MobMasks.Objects.ML_Object_AbsCommandInfo;
import com.marc_little.MobMasks.Objects.ML_Object_CommandReturn;
import com.marc_little.MobMasks.Objects.ML_Object_Permission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class ML_Command_Reload extends ML_Object_AbsCommandInfo {

    public ML_Command_Reload() {
        super("reload", "- Reload all config files", Arrays.asList("r"), new ML_Object_Permission("mobmasks.reload", false));
    }


    @Override
    public ML_Object_CommandReturn executePlayer(Player player, String[] args) {
        //reload here
        return new ML_Object_CommandReturn(ML_Enum_CommandResponse.SUCCESS, "Reloaded configuration files.");
    }

    @Override
    public ML_Object_CommandReturn executeOther(CommandSender sender, String[] args) {
        //reload here
        return new ML_Object_CommandReturn(ML_Enum_CommandResponse.SUCCESS, "Reloaded configuration files.");
    }
}

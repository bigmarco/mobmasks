package com.marc_little.MobMasks.Commands;

import com.marc_little.MobMasks.Enumerators.ML_Enum_CommandResponse;
import com.marc_little.MobMasks.ML_MobMasks;
import com.marc_little.MobMasks.Objects.ML_Object_AbsCommandInfo;
import com.marc_little.MobMasks.Objects.ML_Object_CommandReturn;
import com.marc_little.MobMasks.Objects.ML_Object_Permission;
import com.marc_little.MobMasks.Utils.ML_Config;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class ML_Command_Version extends ML_Object_AbsCommandInfo {

    public ML_Command_Version() {
        super("version", "- Get plugin information", Arrays.asList("v", "ve", "info", "i", "build", "author"), new ML_Object_Permission("mobmasks.version", true));
    }

    @Override
    public ML_Object_CommandReturn executePlayer(Player player, String[] args) {
        player.sendMessage(ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH + "====" + ChatColor.RESET + " " + ML_Config.PREFIX + " " + ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH + "====");
        player.sendMessage(ChatColor.DARK_GRAY + "Name: " + ML_Config.COLOR + ML_MobMasks.NAME);
        player.sendMessage(ChatColor.DARK_GRAY + "Version: " + ML_Config.COLOR + ML_MobMasks.VERSION);
        player.sendMessage(ChatColor.DARK_GRAY + "Author: " + ML_Config.COLOR + ML_MobMasks.AUTHOR);
        player.sendMessage(ChatColor.DARK_GRAY + "Build #: " + ML_Config.COLOR + ML_MobMasks.BUILD_NUMBER);
        player.sendMessage(ChatColor.DARK_GRAY + "Build Date: " + ML_Config.COLOR + ML_MobMasks.BUILD_DATE.toString());
        return new ML_Object_CommandReturn(ML_Enum_CommandResponse.SUCCESS, null);
    }

    @Override
    public ML_Object_CommandReturn executeOther(CommandSender commandSender, String[] args) {
        commandSender.sendMessage(ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH + "====" + ChatColor.RED + ChatColor.BOLD + " " + ML_Config.PREFIX + " " + ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH + "====");
        commandSender.sendMessage(ChatColor.DARK_GRAY + "Name: " + ML_Config.COLOR + ML_MobMasks.NAME);
        commandSender.sendMessage(ChatColor.DARK_GRAY + "Version: " + ML_Config.COLOR + ML_MobMasks.VERSION);
        commandSender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ML_Config.COLOR + ML_MobMasks.AUTHOR);
        commandSender.sendMessage(ChatColor.DARK_GRAY + "Build #: " + ML_Config.COLOR + ML_MobMasks.BUILD_NUMBER);
        commandSender.sendMessage(ChatColor.DARK_GRAY + "Build Date: " + ML_Config.COLOR + ML_MobMasks.BUILD_DATE.toString());
        return new ML_Object_CommandReturn(ML_Enum_CommandResponse.SUCCESS, null);
    }

}

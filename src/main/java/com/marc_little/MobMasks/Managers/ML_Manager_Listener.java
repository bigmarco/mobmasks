package com.marc_little.MobMasks.Managers;

import com.marc_little.MobMasks.ML_MobMasks;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ML_Manager_Listener {

    public ML_Manager_Listener(ML_MobMasks plugin, JarFile jar, ClassLoader classLoader) {
        try {
            Enumeration<JarEntry> entries = jar.entries();
            System.out.println("loading listeners");
            while (entries.hasMoreElements()) {
                String entryName = entries.nextElement().getName();
                if (entryName.contains("$") || !entryName.contains("Listeners") || !entryName.endsWith(".class") || !entryName.contains("ML_Listener_")) continue;
                String className = entryName.replace(".class", "");
                if (entryName.contains("/")) {
                    className = className.replaceAll("/", ".");
                } else if (entryName.contains("\\")) {
                    className = className.replaceAll("\\\\", ".");
                }
                Bukkit.getPluginManager().registerEvents((Listener)classLoader.loadClass(className).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]), plugin);
                System.out.print(className);
            }
            System.out.println("loaded");
        }
        catch (Exception e) {
            e.printStackTrace();
            plugin.getServer().getPluginManager().disablePlugin(plugin);
        }
    }

}

package com.marc_little.MobMasks.Managers;

import com.marc_little.MobMasks.ML_MobMasks;
import com.marc_little.MobMasks.Objects.ML_Object_AbsCommandInfo;
import com.marc_little.MobMasks.Objects.ML_Object_CommandReturn;
import com.marc_little.MobMasks.Objects.ML_Object_Permission;
import com.marc_little.MobMasks.Utils.ML_Config;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ML_Manager_Command implements CommandExecutor {

    private HashMap<String, ML_Object_AbsCommandInfo> commands = new HashMap<String, ML_Object_AbsCommandInfo>();
    private HashMap<String, ML_Object_Permission> helpLines = new HashMap<String, ML_Object_Permission>();
    private ArrayList<ML_Object_AbsCommandInfo> commandInfos = new ArrayList<ML_Object_AbsCommandInfo>();

    public ML_Manager_Command(ML_MobMasks plugin, JarFile jar, ClassLoader classLoader) {
        try {
            Enumeration<JarEntry> entries = jar.entries();
            while (entries.hasMoreElements()) {
                String entryName = entries.nextElement().getName();
                if (entryName.contains("$") || !entryName.contains("Commands") || !entryName.endsWith(".class") || !entryName.contains("ML_Command_")) continue;
                String className = entryName.replace(".class", "");
                if (entryName.contains("/")) {
                    className = className.replaceAll("/", ".");
                } else if (entryName.contains("\\")) {
                    className = className.replaceAll("\\\\", ".");
                }
                commandInfos.add((ML_Object_AbsCommandInfo) classLoader.loadClass(className).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
                System.out.print(className);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            plugin.getServer().getPluginManager().disablePlugin(plugin);
        }
        System.out.println("help loading");
        loadHelp();
    }

    public void loadHelp() {
        for (ML_Object_AbsCommandInfo commandInfo : commandInfos) {
            commands.put(commandInfo.getCommand().toLowerCase(), commandInfo);
            for (String s : commandInfo.getAlias()) {
                commands.put(s.toLowerCase(), commandInfo);
            }
            helpLines.put(ChatColor.DARK_GRAY + "/" + ML_Config.COLOR2 + "mobmasks " + ML_Config.COLOR + commandInfo.getCommand() + " " + ChatColor.GRAY + commandInfo.getDescription(), commandInfo.getMLPermission());
        }
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (args.length == 0) {
            sendHelp(commandSender);
            return true;
        }
        if (args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("h")) {
            sendHelp(commandSender);
            return true;
        }
        if (!commands.containsKey(args[0].toLowerCase())) {
            sendHelp(commandSender);
            return true;
        }
        ML_Object_AbsCommandInfo commandInfo = commands.get(args[0].toLowerCase());
        ML_Object_CommandReturn cmdReturn;
        if (!commandInfo.hasPermission(commandSender)) {
            commandSender.sendMessage(ML_Config.PREFIX + ChatColor.RED + "You Do Not Have Permission To Do This!");
            return true;
        }
        if (commandSender instanceof Player) {
            cmdReturn = commandInfo.executePlayer((Player) commandSender, Arrays.copyOfRange(args, 1, args.length));
        }else {
            cmdReturn = commandInfo.executeOther(commandSender, Arrays.copyOfRange(args, 1, args.length));
        }
        switch (cmdReturn.getResponse()) {
            case SUCCESS:
                if (cmdReturn.getMessage() != null) {
                    commandSender.sendMessage(ML_Config.PREFIX + ChatColor.GREEN + cmdReturn.getMessage());
                }
                return true;
            case HELP:
                sendHelp(commandSender);
                return true;
            case ERROR:
                commandSender.sendMessage(ML_Config.PREFIX + ChatColor.RED + cmdReturn.getMessage());
                return true;
            default:
                return false;
        }
    }

    private void sendHelp(CommandSender commandSender) {
        List<String> helps = new ArrayList<>();
        for (String help : helpLines.keySet()) {
            if (helpLines.get(help).hasPermission(commandSender)) {
                helps.add(help);
            }
        }
        commandSender.sendMessage(ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH + "====" + ChatColor.RESET + " " + ML_Config.PREFIX + " " + ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH + "====");
        commandSender.sendMessage(helps.toArray(new String[helps.size()]));
    }
}

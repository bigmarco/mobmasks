package com.marc_little.MobMasks.Utils;

import com.marc_little.MobMasks.ML_MobMasks;
import com.marc_little.MobMasks.Managers.ML_Manager_MobMasks;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class ML_Config {

    //add config shit here

    public static String PREFIX, COLOR, COLOR2, CONFIG_RELOAD, NO_PERMISSION_COMMAND; //add some more messages here

    //database
    public static String DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD;
    public static int DB_PORT;

    private static YamlConfiguration config;
    private static YamlConfiguration messages;

    public static boolean reload(boolean reloadTasks) {
        ML_MobMasks plugin = ML_MobMasks.getInstance();
        File fileConfig = new File(plugin.getDataFolder(), "config.yml");
        if (!fileConfig.exists()) {
            fileConfig.getParentFile().mkdirs();
            plugin.saveResource("config.yml", true);
        }

        config = new YamlConfiguration();
        try {
            config.load(fileConfig);
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        File fileMsg = new File(plugin.getDataFolder(), "messages.yml");
        if (!fileMsg.exists()) {
            fileMsg.getParentFile().mkdirs();
            plugin.saveResource("messages.yml", true);
        }

        messages = new YamlConfiguration();
        try {
            messages.load(fileMsg);
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        DB_HOST = config.getString("database.host");
        DB_DATABASE = config.getString("database.database");
        DB_USERNAME = config.getString("database.username");
        DB_PASSWORD = config.getString("database.password");
        DB_PORT = config.getInt("database.port");


        ML_Config.PREFIX = color(messages.getString("prefix"));
        ML_Config.COLOR = color(messages.getString("color"));
        ML_Config.COLOR2 = color(messages.getString("color2"));
        ML_Config.CONFIG_RELOAD = color(messages.getString("config-reloaded"));
        ML_Config.NO_PERMISSION_COMMAND = color(messages.getString("no-permission-command"));

        return true;
    }

    public static boolean loadMobMasks(ML_Manager_MobMasks manager) {
        //load custom mob masks here?
        return true;
    }

    private static String color(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }

}
